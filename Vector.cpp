//Writer : Pornapon Punyapan (Na) 580611039 ISNE#3
//This version is already complete version of Vector function test

#include<iostream>
#include<vector>
#include<cstdlib>
#include<ctime>
#include<cmath>

using namespace std;

int num_ele(vector<int> vec); //The number of elements function :  O(constant)
int vec_sum(vector<int> vec,int);//The sum of all elements function : O(n)
int vec_high(vector<int> vec,int);//The highest value function : O(constant)
int vec_low(vector<int> vec);//The lowest value function : O(constant)
float vec_mean(vector<int> vec,int);//The mean value function : O(n)
float vec_med(vector<int> vec,int);//The median value function : O(constant) 
void vec_mode(vector<int> vec,int);//The mode value function : O(n) / maybe O(2n) better
float std_devia(vector<int> vec,int,float);//The standard deviation : O(n)
int num_even(vector<int> vec,int);//The number of even numbers function : O(n)
int num_odd(vector<int> vec,int);//The number of odd numbers function : O(n)
void vec_sort(vector<int> &vec,int);//The sorting function : O(n^2)
void show_num(vector<int> vec,int);//The values output in order from lowest to highest : O(n)


int main()
{
	srand(time(0));
	int time_rand = rand()%99+51;
	vector<int>vec;
	for(int i=0; i<time_rand; i++)
	{
		vec.push_back(i);
		vec[i] = rand()%100+0;
	}
	
	
	cout<<"The number of elements : "<<num_ele(vec)<<endl;
	vec_sort(vec,time_rand);
	cout<<"The sum of all elements : "<<vec_sum(vec,time_rand)<<endl;
	cout<<"The highest value : "<<vec_high(vec,time_rand)<<endl;
	cout<<"The lowest value : "<<vec_low(vec)<<endl;
	cout<<"The mean value : "<<vec_mean(vec,time_rand)<<endl;
	cout<<"The median value : "<<vec_med(vec,time_rand)<<endl;
	cout<<"The mode value : ";
	vec_mode(vec,time_rand);
	cout<<"\nThe standard deviation : "<<std_devia(vec,time_rand,vec_mean(vec,time_rand))<<endl;
	cout<<"The number of even numbers : "<<num_even(vec,time_rand)<<endl;
	cout<<"The number of odd numbers : "<<num_odd(vec,time_rand)<<endl;
	cout<<"The values output in order from lowest to highest."<<endl;
	show_num(vec,time_rand);
	
}

int num_ele(vector<int> vec) //This function used to retrun size of vector
{
	return vec.size();
}

int vec_sum(vector<int> vec,int size) //This function used to calculate summary of all vector
{
	int sum=0;
	for(int i=0; i<size; i++)
	{
		sum = sum+vec[i];
	}	
	return sum;
}

int vec_high(vector<int> vec,int size) //This function used to find (return) the highest value of vector
{	
	return vec[size-1];	
}

int vec_low(vector<int> vec) //This function used to find (return) the lowest value of vector
{	
	return vec[0];	
}

float vec_mean(vector<int> vec,int size) //This function used to calculate the mean of all vector
{
	float vec_sum=0;
	for(int i=0; i<size; i++)
	{
		vec_sum=vec_sum+vec[i];
	}
	return vec_sum/size;	
}

float vec_med(vector<int> vec,int size) //This function used to calculate medain of all vector
{
	int med_num,med_out;
	med_num = size/2;
	if(size%2 ==0)
	{
		med_out=(vec[med_num]+vec[med_num+1])/2;
		return med_out;
	}
	else
	{
		return vec[med_num];
	}		
}

void vec_mode(vector<int> vec,int size) //This function used to calculate mode of all vector
{
	const int cap_dif=size;
	int hold=0,hold_count=0,count_vec[cap_dif];;
	
	for(int i=0; i<size; i++)
	{
		if(vec[i]==vec[i+1])
		{
			hold+=1;
			if(hold>=hold_count)
			{
				hold_count=hold;
				count_vec[i]=hold;
			}
		}
		if(vec[i]!=vec[i+1])
		{
			hold=1;
			count_vec[i]=hold;
		}
	}
	
	for(int i=0; i<size; i++)
	{
		if(count_vec[i]==hold_count)
		{
			cout<<vec[i]<<" ";
		}
	}
	
		
}

float std_devia(vector<int> vec,int size,float avg) //This function used to calculate standard deviation of all vector
{
	float std_dev=0;
	for(int i=0; i<size; i++)
	{
		std_dev = std_dev + pow((avg-vec[i]),2);
	}	
	return sqrt(std_dev);	
}

int num_even(vector<int> vec,int size) //This function used to count how many of even number in this set of vector
{
	int even_num=0;
	for(int i=0; i<size; i++)
	{
		if(vec[i]%2==0)
		{
			even_num+=1;
		}
	}
	return even_num;
}

int num_odd(vector<int> vec,int size) //This function used to count how many of odd number in this set of vector
{
	int odd_num=0;
	for(int i=0; i<size; i++)
	{
		if(vec[i]%2==1)
		{
			odd_num+=1;
		}
	}
	return odd_num;
}

void vec_sort(vector<int> &vec,int size) //This function used to sort the vector
{
	int smallest_number;

	for (int i = 0; i<size-1; i++)
	{
		smallest_number=i;
		for (int j=i+1; j<size; j++)
		{
			if (vec[j] < vec[smallest_number])
			{
				smallest_number=j;
			}	
		}
		swap(vec[i],vec[smallest_number]);
	}
}

void show_num(vector<int> vec,int size) //This function used to display all value in each  vector
{
	int line_count=0;
	for(int i=0; i<size; i++)
	{
		line_count+=1;
		cout<<vec[i]<<" ";
		if(line_count==10)
		{
			cout<<endl;
			line_count=0;
		}
	}	
}